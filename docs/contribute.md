- Send self introduction to the [mailing list](https://lists.centos.org/mailman/listinfo/centos-devel)
  (share some basic info about yourself, your intentions within Storage SIG,
  what way you plan to help the SIG)
- If you want to get access to CentOS Build System (CBS), follow this [HowTo](https://sigs.centos.org/guide/cbs/).
