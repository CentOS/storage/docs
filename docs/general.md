The Storage SIG provides `centos-release-<project>` packages in the **CentOS
Extras repository**. This makes it easy for users to install projects from the
Storage SIG, only two steps are needed, i.e.:

```
# yum install centos-release-gluster
# yum install glusterfs-server
```

Or for example for Ceph:

```
# yum install centos-release-ceph-luminous
# yum install ceph-{mon,osd,...}
```

Available release packages for **_GlusterFS_** that provide associated releases:

- `centos-release-gluster41` (provides GlusterFS-4.1.x and NFS-Ganesha-2.5.x)
- `centos-release-gluster5` (provides GlusterFS-5.x and NFS-Ganesha-2.6.x)
- `centos-release-gluster6` (provides GlusterFS-6.x and NFS-Ganesha-2.7.x)
- `centos-release-gluster7` (provides GlusterFS-7.x)
- `centos-release-gluster8` (provides GlusterFS-8.x)
- `centos-release-gluster9` (provides GlusterFS-9.x)
- `centos-release-gluster10` (provides GlusterFS-10.x)
- `centos-release-gluster11` (provides GlusterFS-11.x)

Available release packages for **_Ceph_**:

- `centos-release-ceph-luminous` (provides Ceph-12.2.y)
- `centos-release-ceph-mimic` (provides Ceph-13.2.y)
- `centos-release-ceph-nautilus` (provides Ceph-14.2.y)
- `centos-release-ceph-octopus` (provides Ceph-15.2.y)
- `centos-release-ceph-pacific` (provides Ceph-16.2.y)
- `centos-release-ceph-quincy` (provides Ceph-17.2.y)
- `centos-release-ceph-reef` (provides Ceph-18.2.y)

Available release packages for **_NFS-Ganesha_** (beginning with NFS-Ganesha-2.8):

- `centos-release-nfs-ganesha28` (provides NFS-Ganesha-2.8.x)
- `centos-release-nfs-ganesha30` (provides NFS-Ganesha-3.x)
- `centos-release-nfs-ganesha4` (provides NFS-Ganesha-4.x)
- `centos-release-nfs-ganesha5` (provides NFS-Ganesha-5.x)

Available release packages for **_Samba_**:

- `centos-release-samba411` (provides Samba-4.11.x)
- `centos-release-samba412` (provides Samba-4.12.x)
- `centos-release-samba413` (provides Samba-4.13.x)
- `centos-release-samba414` (provides Samba-4.14.x)
- `centos-release-samba415` (provides Samba-4.15.x)
- `centos-release-samba416` (provides Samba-4.16.x)
- `centos-release-samba417` (provides Samba-4.17.x)
- `centos-release-samba418` (provides Samba-4.18.x)

Finally:

- `centos-release-storage-common` (base dependency for all Storage SIG release
  packages)

Documentation that describes how to configure and use the projects have their
own pages:

- [Ceph](projects/ceph.md)
- Gluster
- [NFS-Ganesha](projects/nfs-ganesha.md)
- [Samba](projects/samba.md)
