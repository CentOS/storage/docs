Samba is a free SMB and CIFS client and server and Domain Controller for UNIX
and other operating systems.

## Samba in Storage SIG

Samba rpms from Storage SIG are built with internal sources of dependent
libraries for _libtalloc_, _libtevent_, _libtdb_ and _libldb_. Therefore it is
expected to not have any dependency on system installed version of those
libraries. In case you find such an explicit requirement, feel free to report on
[CentOS Bug Tracker](https://bugs.centos.org/).

In addition to standard Samba packages, we ship **samba-vfs-glusterfs** and
**samba-vfs-cephfs** rpms containing VFS module for GlusterFS and CephFS
integration via libgfapi and libcephfs.

**_Note_**:- Packages from Storage SIG does **not** contain necessary elements
to setup Samba as an AD Domain Controller(DC) and are built with system MIT
kerberos requirement for other configurations.

## Installation

Following version streams are available for Samba via Storage SIG:

- centos-release-samba411 on CentOS 7 and CentOS 8 Linux for Samba v4.11.x
- centos-release-samba412 on CentOS 8 Linux for Samba v4.12.x
- centos-release-samba413 on CentOS 8 Linux and CentOS 8 Stream for Samba v4.13.x
- centos-release-samba414 on CentOS 8 Linux, CentOS 8 Stream and CentOS 9 Stream for Samba v4.14.x
- centos-release-samba415 on CentOS 8 Linux, CentOS 8 Stream and CentOS 9 Stream for Samba v4.15.x
- centos-release-samba416 on CentOS 8 Stream and CentOS 9 Stream for Samba v4.16.x
- centos-release-samba417 on CentOS 8 Stream and CentOS 9 Stream for Samba v4.17.x
- centos-release-samba418 on CentOS 8 Stream and CentOS 9 Stream for Samba v4.18.x
- centos-release-samba419 on CentOS 8 Stream and CentOS 9 Stream for Samba v4.19.x
- centos-release-samba420 on CentOS 8 Stream and CentOS 9 Stream for Samba v4.20.x

Use _centos-release-samba package_ to set up repositories for fetching Samba
packages i.e,

```
# yum install centos-release-samba
```

Finally install required Samba rpms.

```
# yum install samba
# yum install samba-vfs-glusterfs samba-vfs-cephfs
```

## Documentation

Refer following sections from Samba Wiki on various documentation details:

- [User Documentation](https://wiki.samba.org/index.php/User_Documentation)
- [Developer Documentation](https://wiki.samba.org/index.php/Developer_Documentation)
- [Contribution](https://wiki.samba.org/index.php/Contribution_documentation)

## GlusterFS Integration

Refer following links from Gluster upstream docs for details on SMB/CIFS access
to GlusterFS volumes:

- [GlusterFS integration](https://docs.gluster.org/en/latest/Administrator%20Guide/Accessing%20Gluster%20from%20Windows/)
- [Client access](https://docs.gluster.org/en/latest/Administrator%20Guide/Setting%20Up%20Clients/#cifs-manual)

## Testing

In general we have Samba's well known selftest configured to be run against each
Merge Request at [GitLab](https://gitlab.com/samba-team/samba/-/merge_requests).

On the other side, we do have nightly CI runs against upstream code base of both
Samba and GlusterFS Mainline Samba GlusterFS integration with following jobs:

- [samba_glusterfs-integration](https://jenkins-samba.apps.ocp.cloud.ci.centos.org/view/GlusterFS/job/samba_glusterfs-integration-test-cases/)
- [samba_cephfs-integration](https://jenkins-samba.apps.ocp.cloud.ci.centos.org/view/CephFS/job/samba_cephfs.vfs-integration-test-cases/)
- [CentOS 8 nightly build job](https://jenkins-samba.apps.ocp.cloud.ci.centos.org/job/samba_build-rpms-centos8-master/)
- [CentOS 9 nightly build job](https://jenkins-samba.apps.ocp.cloud.ci.centos.org/job/samba_build-rpms-centos9-master/)

