Install the latest "release" package to enable the Yum repos:

```
sudo yum -y install centos-release-nfs-ganesha5
```

Install the base package and individual FSALs:

```
sudo yum -y install nfs-ganesha nfs-ganesha-gluster nfs-ganesha-vfs ...
```

## Testing early packages
We push packages to the [testing](https://buildlogs.centos.org/centos/8-stream/storage/x86_64/)
repos before promoting to the main [release](http://mirror.centos.org/centos/8-stream/storage/x86_64/)
repos. You can access them with:

```
sudo yum --enablerepo=centos-nfs-ganesha5-test install nfs-ganesha ...
```

We will let packages sit in testing for a bit before we promote them to the main
repositories.

We want early feedback from testers. Please send feedback to [devel@lists.nfs-ganesha.org](https://lists.nfs-ganesha.org/archives/list/devel@lists.nfs-ganesha.org/)
if you encounter bugs in the testing packages.

## Developer documentation
- [NFS-Ganesha](https://github.com/nfs-ganesha/nfs-ganesha/wiki)
