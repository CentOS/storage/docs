Install the latest "release" package to enable the Yum repos:

```
sudo dnf install -y centos-release-ceph-quincy
```

Now you can install [ceph-ansible](https://github.com/ceph/ceph-ansible) to
deploy ceph on your nodes:

```
sudo dnf install -y ceph-ansible
```

## Testing early packages

We push packages to the testing repos before promoting to the release repos. You
can install from the testing repo with:

```
sudo dnf --enablerepo=centos-ceph-quincy-test install -y ceph
```

We will let packages sit in testing for a bit before we promote them to the main
repositories.

We want early feedback from testers. Please send feedback to [dev@ceph.io](https://lists.ceph.io/hyperkitty/list/dev@ceph.io/)
if you encounter bugs in the testing packages.
