The **CentOS Storage** Special Interest Group (SIG) is a collection of
like-minded individuals coming together to ensure that CentOS is a suitable
platform for many different storage solutions. This group will ensure that all
Open Source storage options seeking to utilize CentOS as a delivery platform
have a voice in packaging, orchestration, deployment, and related work.

## Participating projects

- [Ceph](https://ceph.io/en/)
- [Gluster](https://www.gluster.org/)
- [NFS-Ganesha](https://nfs-ganesha.org)
- [Samba](https://www.samba.org/)
