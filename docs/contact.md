Storage SIG is using centos-devel [mailing list](https://lists.centos.org/mailman/listinfo/centos-devel)
and #centos-devel (IRC channel on [Libera.Chat](https://libera.chat/)) for all
communication. When using the centos-devel mailing list, include [Storage-SIG]
in the subject of the email to get attention from Storage SIG contributors.
